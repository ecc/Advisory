Please visit the [Issues area of this project to
search for, comment and vote on existing issues, and submit new issues](https://git.doit.wisc.edu/ecc/Advisory/issues).  

# ECC

DoIT’s Enterprise Collaboration & Communication (ECC) Services deliver 
a variety of cloud/hybrid enterprise services for general use across campus.  

The portfolio currently includes the large enterprise cloud SaaS services:

- Office 365 (Exchange Online, OneDrive for Business, etc)
- Enterprise email (SMTP relay, anti-spam, etc) 
- WiscChat
- Wisc Account Admin site & domain admin API
- Service Account lifecycle

Tangentally, not directly, related services include:
- Box
- Qualtrics
- Google Suite (Drive, Hangouts, Google Groups, Sites, etc)

## ECC Advisory - Issue Collaboration

The purpose of this project space is to facilitate open collaboration with campus stakeholders
to help define and drive forward common issues, identify shared concerns, and discuss alternative
solutions to pain points in legacy workflows.

DoIT's ECC team will participate in this space by commenting, clarifying, categorizing and moving 
prioritized issues through milestones as work is being completed.

Please visit the [Issues area of this project](https://git.doit.wisc.edu/ecc/Advisory/issues).

## Other resources

[Please contact the Help Desk](https://kb.wisc.edu/helpdesk/) for general troubleshooting and problem resolution.  This ECC Advisory project space will not be used as a support tool.